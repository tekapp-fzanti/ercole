<?php
/*
Plugin Name: Ercole - WPSecurity
Description: Impedisce l'esecuzione di WPScan
Version: 0.0.5
Author: TekApp
Author URI: https://tekapp.it
*/

if (isset($_GET['advanced_fingerprinting'])) {
    switch ($_GET['advanced_fingerprinting']) {
        case '1':
            $file = gzopen(ABSPATH.'wp-includes/js/tinymce/wp-tinymce.js.gz', 'rb');
            $out = '// '.uniqid(true)."\n";
            while(!gzeof($file)) {
                $out .= gzread($file, 4096);
            }

            header('Content-type: application/x-gzip');
            echo gzencode($out);
            break;

        default:
            status_header(404);
    }

    die();
}

if (isset($_GET['plugin_enumeration'])) {
    // Display something random
    die('<!-- ' .uniqid() .'-->');
}

if (!is_admin() && isset($_REQUEST['author'])) {
    status_header(404);
    die();
}

add_action('init', 'anty_wpscan_init');
function anty_wpscan_init() {
    global $wp_version;

    $transient_name = 'wce_block_'.$_SERVER['REMOTE_ADDR'];

    $transient_value = get_transient($transient_name);

    if ($transient_value !== false) {
        die('BANNED!');
    }

    if (isset($_GET['wp_config_enumeration'])) {
        set_transient($transient_name, 1, DAY_IN_SECONDS);
        die('BANNED!');
    }

    // @user_agent = "WPScan v#{WPSCAN_VERSION} (http://wpscan.org)"
    if (!empty($_SERVER['HTTP_USER_AGENT']) && preg_match('/WPScan/i', $_SERVER['HTTP_USER_AGENT'])) {
        die('Wrong user agent');
    }

    // WordPress version identified from stylesheets numbers
    $wp_version = '0001';
}

add_action('do_robots', 'anty_wpscan_do_robots', 1);
function anty_wpscan_do_robots() {
    // Return 404 on robots.txt
    status_header(404);
    die();
}

function add_fake_xmlrpc() {
    // We don't want to display die('XML-RPC server accepts POST requests only.'); on $_GET
    if (!empty($_POST)) {
        return 'wp_xmlrpc_server';
    } else {
        return 'fake_xmlrpc';
    }
}

add_filter('wp_xmlrpc_server_class', 'anty_wpscan_fake_xmlrpc');
class anty_wpscan_fake_xmlrpc {
    function serve_request() {
        // Its fake ;)
        die();
    }
}

// Remove <meta name="generator" content="WordPress" />
remove_action('wp_head', 'wp_generator');
add_filter('the_generator', 'anty_wpscan_remove_generator');
function anty_wpscan_remove_generator() {
    return '';
}

register_activation_hook( __FILE__, 'anty_wpscan_activation');
function anty_wpscan_activation() {
    add_filter('rewrite_rules', 'anty_wpscan_rewrite_rules_filter');
    function anty_wpscan_rewrite_rules_filter($rules){

        $exploded = explode("\n", $rules);

        $my_rules = array('RewriteRule ^readme\.html$ - [R=404,L,NC]', // Disable access to readme.html
            'RewriteRule ^readme\.txt$ - [R=404,L,NC]', // Disable access to readme.txt
            'RewriteRule ^changelog\.txt$ - [R=404,L,NC]', // Disable access to changelog.txt
            'RewriteRule ^wp-includes/rss-functions\.php$ - [R=404,L,NC]', // Disable Full Path Disclosure
            'RewriteRule ^wp-includes/js/tinymce/wp-tinymce\.js\.gz$ index.php?advanced_fingerprinting=1 [L]', // prevent advanced fingerprinting
            'RewriteRule ^(.*)wp-content/plugins/(.*)/readme\.txt$ - [R=404,L]', // Not display plugins readmes
            'RewriteCond %{REQUEST_FILENAME} !-f',
            'RewriteRule ^(.*)wp-content/plugins/(.*)$ index.php?plugin_enumeration=1 [L]', // Always display something when visit plugin dir
            'RewriteRule ^wp-config\.php\.save$ index.php?wp_config_enumeration=1 [L]', // wp-config enumeration
            'RewriteRule ^\.wp-config\.php\.swp$ index.php?wp_config_enumeration=1 [L]',
            'RewriteRule ^wp-config\.php\.swp$ index.php?wp_config_enumeration=1 [L]'
        );

        array_splice( $exploded, 3, 0, $my_rules );

        $rules = implode("\n", $exploded);

        return $rules;
    }

    // Need for save_mod_rewrite_rules
    flush_rewrite_rules(true);
}

register_deactivation_hook( __FILE__, 'anty_wpscan_deactivation');
function anty_wpscan_deactivation() {
    flush_rewrite_rules(true);
}